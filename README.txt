Server & Db metrics

INTRODUCTION
------------

Sys (Server & Db metrics) it's a module that will monitor & display the metrics of servers & database.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/sys

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/sys

REQUIREMENTS
------------

This module using Chartjs library to display charts in Dashboard.

INSTALLATION
------------

1. Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for further
information.

2. Navigate to administer >> modules. Enable Sys.

3. Enjoy & monitor your server : http://example.com/admin/config/development/sys/metrics

CONFIGURATION
-------------

After the installation, there is some settings that you need to edit to receive metrics by email

1. Navigate to administer >> configuration >> development  >> sys dashboards >> settings

2. Enable notification & fill fields

3. Save & wait for your daily or weekly notifications