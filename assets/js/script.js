/**
 * @file
 * Contains js for sys.
 */

(function ($, Drupal, drupalSettings) {
    'use strict';

    Drupal.behaviors.sys = {
        attach: function (context, settings) {
            var db_u = drupalSettings.sys.db_u;
            var labels = labelsTables(db_u);
            var data = dataTables(db_u);
            var ctx = document.getElementById("db-usage-chart").getContext('2d');
            var diskCtx = document.getElementById("disk-chart").getContext('2d');
            var memoryCtx = document.getElementById("memory-chart").getContext('2d');
            var cpuCtx = document.getElementById("cpu-chart").getContext('2d');

            // Show tooltips always even the stats are zero

            Chart.pluginService.register({
              beforeRender: function(chart) {
                if (chart.config.options.showAllTooltips) {
                  // create an array of tooltips
                  // we can't use the chart tooltip because there is only one tooltip per chart
                  chart.pluginTooltips = [];
                  chart.config.data.datasets.forEach(function(dataset, i) {
                    chart.getDatasetMeta(i).data.forEach(function(sector, j) {
                      chart.pluginTooltips.push(new Chart.Tooltip({
                        _chart: chart.chart,
                        _chartInstance: chart,
                        _data: chart.data,
                        _options: chart.options.tooltips,
                        _active: [sector]
                      }, chart));
                    });
                  });

                  // turn off normal tooltips
                  chart.options.tooltips.enabled = false;
                }
              },
              afterDraw: function(chart, easing) {
                if (chart.config.options.showAllTooltips) {
                  // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                  if (!chart.allTooltipsOnce) {
                    if (easing !== 1)
                      return;
                    chart.allTooltipsOnce = true;
                  }

                  // turn on tooltips
                  chart.options.tooltips.enabled = true;
                  Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
                    tooltip.initialize();
                    tooltip.update();
                    // we don't actually need this since we are not animating tooltips
                    tooltip.pivot();
                    tooltip.transition(easing).draw();
                  });
                  chart.options.tooltips.enabled = false;
                }
              }
            });

            // Show tooltips always even the stats are zero

            var tablesChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        label: Drupal.t(' Table size in MB'),
                        data: data,
                        backgroundColor : '#364f6b',
                        hoverBackgroundColor: 'rgb(255, 99, 132)',
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });


            var cpu_u = drupalSettings.sys.cpu_u.toFixed(2);
            data = [cpu_u, 100 - cpu_u]
            var cpuChart = new Chart(cpuCtx, {
                type: 'pie',
                data: {
                    labels: ['Used space', 'Available space'],
                    datasets: [{
                        label: "GB",
                        backgroundColor: ["#fc5185", "#364f6b"],
                        data: data
                    }]
                },
                options: {
                  // In options, just use the following line to show all the tooltips
                  showAllTooltips: true,
                  tooltips: {
                    backgroundColor: '#FFF',
                    titleFontColor: '#0066ff',
                    bodyFontColor: '#000',
                  }
                }
            });


            var disk_u = drupalSettings.sys.disk_u;
            data = dataDisk(disk_u);
            var diskChart = new Chart(diskCtx, {
                type: 'pie',
                data: {
                    labels: ['Free space', 'Used space'],
                    datasets: [{
                        label: "GB",
                        backgroundColor: ["#fc5185", "#364f6b"],
                        data: data
                    }]
                },
                options: {
                  // In options, just use the following line to show all the tooltips
                  showAllTooltips: true,
                  tooltips: {
                    backgroundColor: '#FFF',
                    titleFontColor: '#0066ff',
                    bodyFontColor: '#000',
                  }
                }
            });


            var memory_u = drupalSettings.sys.memory_u;
            data = dataMemory(memory_u);
            var memoryChart = new Chart(memoryCtx, {
                type: 'doughnut',
                data: {
                    labels: ['Free space', 'Used space', 'Cached space'],
                    datasets: [{
                        backgroundColor: ["#fc5185", "#364f6b"],
                        label: 'GB',
                        data: data
                    }]
                },
                options: {
                  // In options, just use the following line to show all the tooltips
                  showAllTooltips: true,
                  tooltips: {
                    backgroundColor: '#FFF',
                    titleFontColor: '#0066ff',
                    bodyFontColor: '#000',
                  }
                }
            });

        }
    }
})(jQuery, Drupal, drupalSettings);

var labelsTables = function ($data) {
    return $data.map(function (e) {
        return e.name;
    });
};

var dataTables = function ($data) {
    return $data.map(function (e) {
        return (e.size_byte / 1048576).toFixed(2);
    });
};
var dataDisk = function ($data) {
    var $array = $data[0];
    if($array.length > 4) {
        $array.pop();
    }
    var data = $array.filter(function (e) {
        return (e.indexOf('G') || e.indexOf('M')) !== -1;
    });
    return data.map(function (e) {
        if(e.indexOf('G') !== -1) {
            return e.replace("G", '');
        }
        else if(e.indexOf('M') !== -1) {
            return e.replace("M", '');
        }
    });
};

var dataMemory = function ($data) {
    var $ram = $data[1];
    var data = [];
    var str = null;

    data[0] = $ram.free;
    data[1] = $ram.used;
    data[2] = $ram.cached;

    data = data.map(function (e) {
        if(e.indexOf('Gi') !== -1) {
            return e.replace("Gi", '');
        }
        if(e.indexOf('Mi') !== -1) {
            str = e.replace("Mi", '');
            return (str / 1024).toFixed(2);
        }
    });
    return data;
};
