<?php

/**
 * @file
 * Contains \Drupal\sys\Form\SysSettingsForm.
 */

namespace Drupal\sys\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SysSettingsForm extends ConfigFormBase {

	/**
	 * {@inheritdoc}
	 */
	public function getFormId() {
		return 'sys_settings_form';
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {
		$config = $this->config('sys.settings');

		$form['get_notified'] = [
			'#type' => 'checkbox',
			'#title' => t('Sys notification.'),
			'#default_value' => $config->get('get_notified'),
			'#description' => $this->t("Check if you want to get email notification for server disk    and memory."),
		];

		$form['email_to'] = [
			'#type' => 'textarea',
			'#title' => $this->t("Email to"),
			'#default_value' => $config->get('email_to'),
			'#description' => $this->t("Enter multiple email address to get notification email. Please enter email address one by one without adding comma at the end."),
		];


		$form['cron'] = [
			'#type' => 'select',
			'#title' => $this->t("Sys notification cron"),
			'#default_value' => $config->get('cron'),
			'#description' => $this->t("Check if you want to get email notification for Sys for schedule cron. By default it runs according to site predefined cron schedule."),
			'#options' => [
				'daily' => t('Daily'),
				'weekly' => t('Weekly'),
			],
		];

    $form['memory_spaces'] = [
      '#type' => 'select',
      '#title' => $this->t("Sys memory_spaces"),
      '#default_value' => $config->get('memory_spaces'),
      '#description' => $this->t("If you chose default , value 25% will be selected."),
      '#options' => [
        'default'=> 'default',
        50 => t('50'),
        70 => t('70'),
        80 => t('80'),
        90 => t('90'),
      ],
    ];

    $form['cpu_space'] = [
      '#type' => 'select',
      '#title' => $this->t("Sys cpu_space"),
      '#default_value' => $config->get('cpu_space'),
      '#description' => $this->t("If you chose default , value 25% will be selected."),
      '#options' => [
        'default'=> 'default',
        50 => t('50'),
        70 => t('70'),
        80 => t('80'),
        90 => t('90'),
      ],
    ];
    $form['disk_space'] = [
      '#type' => 'select',
      '#title' => $this->t("Sys disk_space"),
      '#default_value' => $config->get('disk_space'),
      '#description' => $this->t("If you chose default , value 75% will be selected."),
      '#options' => [
        'default'=> 'default',
        50 => t('50'),
        70 => t('70'),
        80 => t('80'),
        90 => t('90'),
      ],
    ];

		return parent::buildForm($form, $form_state);
	}

	/**
	 * Checks form email_to & email fields.
	 *
	 * @param array $form
	 * @param \Drupal\Core\Form\FormStateInterface $form_state
	 */
	public function validateServerdiscspace(array &$form, FormStateInterface $form_state) {
		$email_to = explode("\n", $form_state->getValue('email_to'));

		foreach ($email_to as $key => $value) {
			if (!empty($value)) {
				if (FALSE == filter_var(trim($value), FILTER_VALIDATE_EMAIL)) {
					$form_state->setErrorByName('email_to', $this->t('Invalid email address -- @value', ['@value' => trim($value)]));
				}
			}
		}

		$email_from_address = trim($form_state->getValue('email_from_address'));
		if (empty($email_from_address)) {
			$form_state->setErrorByName('email_from_address', $this->t('Invalid email address -- @value', ['@value' => trim($email_from_address)]));
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
		// Retrieve the configuration.
		$this->config('sys.settings')
			// Set the submitted configuration setting.
			->set('get_notified', $form_state->getValue('get_notified'))
			->set('email_to', $form_state->getValue('email_to'))
			->set('cron', $form_state->getValue('cron'))
      ->set('cpu_space', $form_state->getValue('cpu_space'))
      ->set('disk_space', $form_state->getValue('disk_space'))
      ->set('memory_spaces', $form_state->getValue('memory_spaces'))
			->save();

		parent::submitForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	protected function getEditableConfigNames() {
		return ['sys.settings'];
	}

}