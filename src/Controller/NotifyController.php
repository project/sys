<?php

/**
 * @file
 * Contains \Drupal\optimizedb\Controller\HideNotificationController.
 */

namespace Drupal\sys\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Page Notify.
 */
class NotifyController extends ControllerBase {

  /**
   * Page hide notification.
   *
   * @return string
   *   Result hide notification.
   */
  public function notify() {
    //@todo notifications system
  }

}
