<?php

/**
 * @file
 * Contains \Drupal\sys\SysService.
 */

namespace Drupal\sys;

use Symfony\Component\HttpFoundation\Request;

class SysService {

  /**
   * Get metrics in html tables.
   *
   * @return mixed|null|string
   */
  public function getTables() {
    $render = \Drupal::service('renderer');
    $size = _sys_database_size();
    $tables = _sys_tables_list();
    $disk_table = _sys_disk_usage();
    $request = Request::createFromGlobals();

    $headers = array(
      'name' => array(
        'data' => t('Table name'),
      ),
      'size' => array(
        'data' => t('Table size'),
      ),
    );
    $sort = \Drupal\Core\Utility\TableSort::getSort($headers, $request);
    usort($tables, function($a, $b) use ($sort) {
      return $b['size_byte'] - $a['size_byte'] ;
    });

    $rows = array();
    $tables = array_slice($tables, 0, 10);

    foreach ($tables as $table) {
      unset($table['size_byte']);

      $rows[$table['name']] = $table;
    }
    $build['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No metrics available.'),
      '#prefix' => "<h2>The size of all tables in the database : {$size['size']}</h2><h4>hit 10 big tables size</h4>",
    ];

    $headers = [
      'partition' => t('Partition name'),
      'percentage' => t('%'),
      'free_space' => t('Free Space'),
      'used_space' => t('Used space'),
      'partition_size' => t('Total'),
    ];

    $rows = [];

    foreach ($disk_table as $table) {
      $rows[$table[0]] = [
        'partition' => $table[0],
        'percentage' => $table[1],
        'free_space' => $table[2],
        'used_space' => $table[3],
        'partition_size' => $table[4],
      ];
    }

    $build['disk'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No metrics available.'),
    ];

    return $render->renderPlain($build);
  }

  /**
   * Get Disk space percentage.
   *
   * @return mixed|null|string
   */
  public function getDiskSpace() {
    $disk_table = _sys_disk_usage();
    return $disk_table[0][1];
  }

  /**
   * Get CPU space percentage.
   *
   * @return mixed|null|string
   */
  public function getServerLoadLinuxData() {
    if (is_readable("/proc/stat"))
    {
      $stats = @file_get_contents("/proc/stat");

      if ($stats !== false)
      {
        // Remove double spaces to make it easier to extract values with explode()
        $stats = preg_replace("/[[:blank:]]+/", " ", $stats);

        // Separate lines
        $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
        $stats = explode("\n", $stats);

        // Separate values and find line for main CPU load
        foreach ($stats as $statLine)
        {
          $statLineData = explode(" ", trim($statLine));

          // Found!
          if
          (
            (count($statLineData) >= 5) &&
            ($statLineData[0] == "cpu")
          )
          {
            return array(
              $statLineData[1],
              $statLineData[2],
              $statLineData[3],
              $statLineData[4],
            );
          }
        }
      }
    }

    return null;
  }

  function getCpuSpace()
  {
    $load = null;

    if (stristr(PHP_OS, "win"))
    {
      $cmd = "wmic cpu get loadpercentage /all";
      @exec($cmd, $output);

      if ($output)
      {
        foreach ($output as $line)
        {
          if ($line && preg_match("/^[0-9]+\$/", $line))
          {
            $load = $line;
            break;
          }
        }
      }
    }
    else
    {
      if (is_readable("/proc/stat"))
      {
        // Collect 2 samples - each with 1 second period
        // See: https://de.wikipedia.org/wiki/Load#Der_Load_Average_auf_Unix-Systemen
        $statData1 = $this->getServerLoadLinuxData();
        sleep(1);
        $statData2 = $this->getServerLoadLinuxData();

        if
        (
          (!is_null($statData1)) &&
          (!is_null($statData2))
        )
        {
          // Get difference
          $statData2[0] -= $statData1[0];
          $statData2[1] -= $statData1[1];
          $statData2[2] -= $statData1[2];
          $statData2[3] -= $statData1[3];

          // Sum up the 4 values for User, Nice, System and Idle and calculate
          // the percentage of idle time (which is part of the 4 values!)
          $cpuTime = $statData2[0] + $statData2[1] + $statData2[2] + $statData2[3];

          // Invert percentage to get CPU time, not idle time
          $load = 100 - ($statData2[3] * 100 / $cpuTime);
        }
      }
    }

    return $load;
  }

  /**
   * Get Memory space percentage.
   *
   * @return mixed|null|string
   */
  public function getMemorySpace($getPercentage=true) {
    $memoryTotal = null;
    $memoryFree = null;

    if (stristr(PHP_OS, "win")) {
      // Get total physical memory (this is in bytes)
      $cmd = "wmic ComputerSystem get TotalPhysicalMemory";
      @exec($cmd, $outputTotalPhysicalMemory);

      // Get free physical memory (this is in kibibytes!)
      $cmd = "wmic OS get FreePhysicalMemory";
      @exec($cmd, $outputFreePhysicalMemory);

      if ($outputTotalPhysicalMemory && $outputFreePhysicalMemory) {
        // Find total value
        foreach ($outputTotalPhysicalMemory as $line) {
          if ($line && preg_match("/^[0-9]+\$/", $line)) {
            $memoryTotal = $line;
            break;
          }
        }

        // Find free value
        foreach ($outputFreePhysicalMemory as $line) {
          if ($line && preg_match("/^[0-9]+\$/", $line)) {
            $memoryFree = $line;
            $memoryFree *= 1024;  // convert from kibibytes to bytes
            break;
          }
        }
      }
    }
    else
    {
      if (is_readable("/proc/meminfo"))
      {
        $stats = @file_get_contents("/proc/meminfo");

        if ($stats !== false) {
          // Separate lines
          $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
          $stats = explode("\n", $stats);

          // Separate values and find correct lines for total and free mem
          foreach ($stats as $statLine) {
            $statLineData = explode(":", trim($statLine));

            //
            // Extract size (TODO: It seems that (at least) the two values for total and free memory have the unit "kB" always. Is this correct?
            //

            // Total memory
            if (count($statLineData) == 2 && trim($statLineData[0]) == "MemTotal") {
              $memoryTotal = trim($statLineData[1]);
              $memoryTotal = explode(" ", $memoryTotal);
              $memoryTotal = $memoryTotal[0];
              $memoryTotal *= 1024;  // convert from kibibytes to bytes
            }

            // Free memory
            if (count($statLineData) == 2 && trim($statLineData[0]) == "MemFree") {
              $memoryFree = trim($statLineData[1]);
              $memoryFree = explode(" ", $memoryFree);
              $memoryFree = $memoryFree[0];
              $memoryFree *= 1024;  // convert from kibibytes to bytes
            }
          }
        }
      }
    }

    if (is_null($memoryTotal) || is_null($memoryFree)) {
      return null;
    } else {
      if ($getPercentage) {
        return (100 - ($memoryFree * 100 / $memoryTotal));
      } else {
        return array(
          "total" => $memoryTotal,
          "free" => $memoryFree,
        );
      }
    }
  }
}
